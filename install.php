<?php

include_once 'Internal_Functions/Functions.php';

$articlesTable = Functions::getInstance()->getTable('Articles');
$usersTable = Functions::getInstance()->getTable('Users');
$sourcesTable = Functions::getInstance()->getTable('Sources');

$command = "SET SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO';";

$query = mysqli_query($connection, $command) or die(print "Error: Could not create tables! Check _settings.php is correct!");

$command = "SET time_zone = '+00:00';";

$query = mysqli_query($connection, $command) or die(print "Error: Could not create tables! Check _settings.php is correct!");

$command = "CREATE TABLE IF NOT EXISTS `$articlesTable` (
  `ArticleID` int(11) NOT NULL AUTO_INCREMENT,
  `SourceID` int(11) NOT NULL,
  `Title` char(100) NOT NULL,
  `Description` text NOT NULL,
  `Link` text NOT NULL,
  `Timestamp` datetime NOT NULL,
  `Image` text,
  `EditTitle` char(100) DEFAULT NULL,
  `EditDescription` text,
  `EditImage` text,
  `Removed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ArticleID`),
  UNIQUE KEY `uq_Articles` (`Title`,`SourceID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";

$query = mysqli_query($connection, $command) or die(print "Error: Could not create tables! Check _settings.php is correct!");

$command = "CREATE TABLE IF NOT EXISTS `$sourcesTable` (
  `SourceID` int(11) NOT NULL AUTO_INCREMENT,
  `SourceName` char(64) NOT NULL,
  `Type` int(11) NOT NULL,
  `Location` text NOT NULL,
  `LastChecked` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Interval` int(11) NOT NULL DEFAULT '10',
  PRIMARY KEY (`SourceID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";

$query = mysqli_query($connection, $command) or die(print "Error: Could not create tables! Check _settings.php is correct!");

$command = "CREATE TABLE IF NOT EXISTS `$usersTable` (
  `User_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Username` char(32) NOT NULL,
  `Password` char(64) NOT NULL,
  `SuperAdmin` tinyint(1) NOT NULL,
  PRIMARY KEY (`User_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";

$query = mysqli_query($connection, $command) or die(print "Error: Could not create tables! Check _settings.php is correct!");

$newUsername = "Admin";
$newUsername = stripslashes($newUsername);
$newUsername = Functions::getInstance()->getConnection()->real_escape_string($newUsername);

$newPassword = hash("sha256", "Admin");
$newPassword = stripslashes($newPassword);
$newPassword = Functions::getInstance()->getConnection()->real_escape_string($newPassword);

$command = "INSERT INTO " . $usersTable . " (Username, Password, SuperAdmin) VALUES ('$newUsername', '$newPassword', 1);";
$query = mysqli_query($connection, $command) or die();

print "Setup Successful! Login with Username Admin, Password Admin! Please delete install.php!";