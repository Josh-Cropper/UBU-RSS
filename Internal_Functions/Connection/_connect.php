<?php
include_once '_settings.php';

    session_start(); //Begins the user's session, which is used to store their userdata, and identifies them when multiple users are accessing the webapp.

    $connection = new mysqli(
            $_SETTINGS['Host'],
            $_SETTINGS['Username'],
            $_SETTINGS['Password'],
            $_SETTINGS['Database'],
            $_SETTINGS['Port']) or die("Error: Unable to connect to database:" . PHP_EOL);
