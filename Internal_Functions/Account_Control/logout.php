<?php
/*
 * Log out method
 */

include_once '../Connection/_connect.php';

session_destroy(); //Ends the session for the connection accessing the page, and removes any information stored in the $_SESSION global array.

header("Location: /"); // Directs the user back to the login page.
