<?php
/*
 * Login Checks
 */
include_once '../Connection/_settings.php';
include_once '../Connection/_connect.php';

$usersTable = $_TABLE['Users'];

if (isset($_POST['login'])) {
    $username = $_POST['username'];
    $password = hash("sha256", $_POST['password']);

    $username = stripslashes($username);
    $username = $connection->real_escape_string($username);

    $password = stripslashes($password);
    $password = $connection->real_escape_string($password);
    
    $command = "SELECT * FROM $usersTable WHERE Username = '$username'";
    $query = mysqli_query($connection, $command);
    $result = mysqli_fetch_row($query);
    
    $userExists = mysqli_num_rows($query) == 1;
    
    if(!$userExists){
        $_SESSION['Error'] = array(false, "Incorrect Username! $username");
        header("Location: /Login/");
    }
    
    if($result[2] == $password){
        $_SESSION['User'] = array(
            $result[0],
            $result[1],
            $result[2],
            $result[3]
        );
        header("Location: /");
    } else {
        $_SESSION['Error'] = array(false, "Incorrect Password!");
        header("Location: /Login/");
    }
} else {
    header("Location: /Login/");
}
