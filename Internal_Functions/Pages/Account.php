<div class="row">
    
    <div class="col-md-2 hidden-xs hidden-sm"></div>
    <div class="col-xs-12 col-md-4">
        <div class="content" style="text-align: center;">
            <h3 class="content-title">Change Username</h3>
            <form class="form-horizontal" action="/Internal_Functions/Functions.php?Function=changeUsername" method="post">
                <input type="hidden" name="UserID" value="<?php print $_SESSION['User'][0]; ?>" />
                <input class="form-control input-lg" type="text"
                       name="newUsername" placeholder="New Username" required readonly onfocus="$(this).removeAttr('readonly');"/>
                <input class="form-control input-lg" type="password"
                       name="Password" placeholder="Password" required readonly onfocus="$(this).removeAttr('readonly');"/>
                <button type="submit" class="btn btn-primary" name="changeUsername">Submit</button>
            </form>
        </div>
    </div>
    <div class="col-xs-12 col-md-4">
        <div class="content" style="text-align: center;">
            <h3 class="content-title">Change Password</h3>
            <form class="form-horizontal" action="/Internal_Functions/Functions.php?Function=changePassword" method="post">
                <input type="hidden" name="UserID" value="<?php print $_SESSION['User'][0]; ?>" />
                <input class="form-control input-lg" type="text"
                       name="newPassword" placeholder="New Password" required readonly onfocus="$(this).removeAttr('readonly');"/>
                <input class="form-control input-lg" type="password"
                       name="Password" placeholder="Password" required readonly onfocus="$(this).removeAttr('readonly');"/>
                <button type="submit" class="btn btn-primary" name="changePassword">Submit</button>
            </form>
        </div>
    </div>
    <div class="col-md-2 hidden-xs hidden-sm"></div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="content">
            <h3 class="content-title">Users</h3>
            <table class="table table-striped table-condensed" style>
                <thead>
                <th>User ID</th>
                <th>Username</th>
                <th>Super Admin</th>
                <th>Remove</th>
                </thead>
                <tbody>
                    <?php
                        $usersDetails = Functions::getInstance()->getUsers();
                        foreach ($usersDetails as $userDetails) {
                            $admin = $userDetails[3] == 1 ? "<span class='glyphicon glyphicon-ok' style='color:green'></span>" : "<span class='glyphicon glyphicon-remove' style='color:red'></span>";
                            print
"<tr>
    <td>$userDetails[0]</td>
    <td>$userDetails[1]</td>
    <td>$admin</td>
    <td>";
        if($_SESSION['User'][3] == 1){
            print
"       <button type='button' class='btn btn-danger' data-toggle='modal' href='#removeUser$userDetails[0]'>&times;</button>";
        } else {
            print
"       <button type='button' class='btn btn-danger disabled'>&times;</button>";
        }
            print
"   </td>
</tr>";
            print
"       <div class='modal fade' id='removeUser$userDetails[0]' tabindex='-1' role='dialog' aria-labelledby='removeUser$userDetails[0]' aria-hidden='true'>
            <div class='modal-dialog modal-sm'>
                <div class='modal-content' style='margin-top: 68px;'>
                    <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                        <p class='modal-title'>Are you sure?</p>
                    </div>
                    <div class='modal-body'>
                        <form name='removeUserForm' method='post' action='/Internal_Functions/Functions.php?Function=removeUser'>
                            <input type='hidden' name='userID' value='$userDetails[0]'>
                            <button class='btn btn-danger' type='submit' name='removeUser'>Remove</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>";
                        }
                    ?>
                </tbody>
            </table>
            <div class='row'>
            <div class='col-xs-11'></div>
            <div class='col-xs-1'>
                <button style="float: right;" type="button" class="btn btn-success" data-toggle="modal" href="#addUser"><strong>+</strong></span></button>
            </div>
            </div>
        </div>
    </div>
</div>

        <div class='modal fade' id='addUser' tabindex='-1' role='dialog' aria-labelledby='addUser' aria-hidden='true'>
            <div class='modal-dialog modal-sm'>
                <div class='modal-content' style='margin-top: 68px;'>
                    <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                        <p class='modal-title'>Add New User</p>
                    </div>
                    <div class='modal-body'>
                        <form class="form-signin" action="/Internal_Functions/Functions.php?Function=addUser" method="post" name="newUserForm">
                            <input type="hidden" name="creatorID" value="<?php print $_SESSION['User'][0]; ?>">
                            <input class="form-control" type="text" name="newUser" placeholder="New Username" required>
                            <input class="form-control" type="password" name="Password" placeholder="New User's Password" required>
                            <script>
                                $(document).ready(function () {
                                    toggleFields();
                                    $('#Administrator').change(function () {
                                        toggleFields();
                                    });
                                });

                                function toggleFields() {
                                    if ($('#Administrator').val() == '1')
                                        $('#passwordDiv').show();
                                    else
                                        $('#passwordDiv').hide();
                                }
                            </script>
                            <select name='Administrator' id='Administrator' class='form-control' style="margin-bottom: 5px;">
                                <option value="0">Non-Administrator</option>
                                <option value="1">Administrator</option>
                            </select>
                            <div id='passwordDiv' style="margin-bottom: 5px;">
                                <input class='form-control' type='password' name='currentPassword' placeholder='Current Password'>
                            </div>
                            <button style='margin-top: -2px;' class="btn btn-primary" type="addUser">Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>