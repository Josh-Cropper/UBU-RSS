<?php

$articles = Functions::getInstance()->getArticles(null, Functions::getInstance()->getSourceIDs());
foreach ($articles as $article) {
$title = $article[6] == true ? $article[7] : $article[0];
$description = $article[6] == true ? $article[8] : $article[1];
$sourceName = Functions::getInstance()->getSourceName($article[5]);
$date = date('d/m/Y H:i:s', $article[3]);
if ($article[6] && $article[10]) {
print
"<div class='col-xs-12 col-md-4 col-lg-3'>
        <div class='content hoverable removed'>
            <div class='dropdown col-xs-2'>
            <button style='position: absolute; left: 5px;' class='btn btn-success' type='button' data-toggle='dropdown'>
            <span class='glyphicon glyphicon-link'></span></button>
            <ul class='dropdown-menu' style='text-align: center; top: 35px;'>
                <li class='dropdown-header'>Go to link?</li>
                <li class='divider'></li>
                <li>
                    <form action='$article[2]' method=''>
                        <button type='submit' class='btn btn-primary'>Go</button>
                    </form>
                </li>
            </ul>
            </div>
            <h3 class='content-title article col-xs-8'>$title</h3>
                <div class='dropdown col-xs-2'>
                    <button style='position: absolute; right: 5px;' class='btn btn-danger dropdown-toggle' type='button' data-toggle='dropdown'>
                    <span class='glyphicon glyphicon-pencil'></span></button>
                    <ul class='dropdown-menu' style='text-align: center; top: 35px; transform: translateX(-70px);'>
                        <li class='dropdown-header'>Edit Article?</li>
                        <li class='divider'></li>
                        <li>
                            <button class='btn btn-primary' type='button' data-toggle='modal' href='#remArticle$article[5]$article[3]'>Undo Remove</button>
                        </li>
                    </ul>
                </div>
            <p class='subtext col-xs-12'>$description</p>
            <p style='position: absolute; margin-top: 160px; vertical-align: bottom; text-align: left;' class='subtext'>Source: $sourceName</p>
            <p style='position: absolute; float: right; text-align: right; margin-top: 140px;' class='subtext'>";
print $date;
print "</p>
        </div>

<div class='modal fade' id='remArticle$article[5]$article[3]' tabindex='-1' role='dialog' aria-labelledby='remArticle$article[5]$article[3]' aria-hidden='true'>
    <div class='modal-dialog modal-sm'>
        <div class='modal-content' style='margin-top: 60px;'>
            <div class='modal-header'>
                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                <p class='modal-title'>Undo Remove?</p>
            </div>
            <div class='modal-body'>
                <form class='form form-signin' name='Undo' method='post' action='/Internal_Functions/Functions.php?Function=remArticle'>
                    <input type='hidden' name='sourceID' value='$article[5]' />
                    <input type='hidden' name='Title' value='$article[0]' />
                    <button class='btn btn-danger' type='submit' name='Undo'>Undo</button>
                </form>
            </div>
        </div>
    </div>
</div>

</div>";
} else {
    print
            "<div class='col-xs-12 col-md-4 col-lg-3'>
        <div class='content hoverable'>
            <div class='dropdown col-xs-2'>
            <button style='position: absolute; left: 5px;' class='btn btn-success' type='button' data-toggle='dropdown'>
            <span class='glyphicon glyphicon-link'></span></button>
            <ul class='dropdown-menu' style='text-align: center; top: 35px;'>
                <li class='dropdown-header'>Go to link?</li>
                <li class='divider'></li>
                <li>
                    <form action='$article[2]' method=''>
                        <button type='submit' class='btn btn-primary'>Go</button>
                    </form>
                </li>
            </ul>
            </div>
            <h3 class='content-title article col-xs-8'>$title</h3>
                <div class='dropdown col-xs-2'>
                    <button style='position: absolute; right: 5px;' class='btn btn-danger dropdown-toggle' type='button' data-toggle='dropdown'>
                    <span class='glyphicon glyphicon-pencil'></span></button>
                    <ul class='dropdown-menu' style='text-align: center; top: 35px; transform: translateX(-70px);'>
                        <li class='dropdown-header'>Edit Article?</li>
                        <li class='divider'></li>
                        <li>
                            <button class='btn btn-primary' type='button' data-toggle='modal' href='#editArticle$article[5]$article[3]'>Edit</button>
                        </li>
                    </ul>
                </div>
            <p class='subtext col-xs-12'>$description</p>
            <p style='position: absolute; margin-top: 160px; vertical-align: bottom; text-align: left;' class='subtext'>Source: $sourceName</p>
            <p style='position: absolute; float: right; text-align: right; margin-top: 140px;' class='subtext'>";
    print $date;
    print "</p>
        </div>
        

<div class='modal fade' id='editArticle$article[5]$article[3]' tabindex='-1' role='dialog' aria-labelledby='editArticle$article[5]$article[3]' aria-hidden='true'>
    <div class='modal-dialog modal-lg'>
        <div class='modal-content' style='margin-top: 60px;'>
            <div class='modal-header'>
                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                <p class='modal-title'>Edit Article</p>
            </div>
            <div class='modal-body'>
                <form class='form form-signin' name='editArticle' method='post' action='/Internal_Functions/Functions.php?Function=editArticle'>
                    <input type='hidden' name='sourceID' value='$article[5]' />
                    <input type='hidden' name='Title' value='$article[0]' />
                    <input class='form-control input-lg' type='text' placeholder='Title' name='editTitle' value='$title' />
                    <input type='hidden' name='Description' value='$article[1]' />
                    <textarea style='margin-bottom: 5px;' class='form-control input-lg' cols='40' rows='5' placeholder='Description...' name='editDescription'>$description</textarea>
                    <input type='hidden' name='Image' value='$article[4]' />
                    <input class='form-control input-lg' type='text' name='editImage' value='$article[4]' placeholder='Image' />
                    <input type='hidden' name='Link' value='$article[2]' />
                    <input type='hidden' name='Timestamp' value='";
    print date('Y-m-d H:i:s', $article[3]);
    print "' />
                    <button class='btn btn-danger' type='submit' name='editArticle'>Finish</button>
                    <button class='btn btn-danger' style='float: right;' type='button' data-toggle='modal' href='#remArticle$article[5]$article[3]'>Remove</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class='modal fade' id='remArticle$article[5]$article[3]' tabindex='-1' role='dialog' aria-labelledby='remArticle$article[5]$article[3]' aria-hidden='true'>
    <div class='modal-dialog modal-sm'>
        <div class='modal-content' style='margin-top: 60px;'>
            <div class='modal-header'>
                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                <p class='modal-title'>Remove Article?</p>
            </div>
            <div class='modal-body'>
                <form class='form form-signin' name='remArticle' method='post' action='/Internal_Functions/Functions.php?Function=remArticle'>
                    <input type='hidden' name='sourceID' value='$article[5]' />
                    <input type='hidden' name='Title' value='$article[0]' />
                    <input type='hidden' name='Description' value='$article[1]' />
                    <input type='hidden' name='Image' value='$article[4]' />
                    <input type='hidden' name='Timestamp' value='";
    print date('Y-m-d H:i:s', $article[3]);
    print "' />
                    <input type='hidden' name='Link' value='$article[2]' />
                    <button class='btn btn-danger' type='submit' name='remArticle'>Remove</button>
                </form>
            </div>
        </div>
    </div>
</div>

</div>";
}
}