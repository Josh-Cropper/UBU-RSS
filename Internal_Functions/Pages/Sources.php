<?php
$sources = Functions::getInstance()->getSources();
foreach ($sources as $source) {
    print
"<div class='col-xs-12 col-md-6 col-lg-4'>
        <div class='content hoverable'>
            <div class='col-xs-2'></div>
            <h3 class='content-title col-xs-8'>$source[1]</h3>
                <div class='dropdown col-xs-2'>
                    <button style='position: absolute; right: 5px;' class='btn btn-danger dropdown-toggle' type='button' data-toggle='dropdown'>&times;</button>
                    <ul class='dropdown-menu' style='text-align: center; top: 35px; transform: translateX(-70px);'>
                        <li class='dropdown-header'>Remove Source?</li>
                        <li class='divider'></li>
                        <li>
                            <form action='/Internal_Functions/Functions.php?Function=removeSource' method='post'>
                                <input type='hidden' name='data' value='$source[0]' />
                                <button type='submit' class='btn btn-primary'>Remove</button>
                            </form>
                        </li>
                    </ul>
                </div>
            <p class='subtext col-xs-12'>$source[3]</p>
        </div>
</div>

<div class='modal fade' id='remove$source[0]' tabindex='-1' role='dialog' aria-labelledby='remove$source[0]' aria-hidden='true'>
    <div class='modal-dialog modal-sm'>
        <div class='modal-content' style='margin-top: 68px;'>
            <div class='modal-header'>
                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                <p class='modal-title'>Remove Source Feed?</p>
            </div>
            <div class='modal-body'>
                <div style='text-align: center;'>
                    <a href='/Internal_Functions/Functions.php?Function=removeSource&data=$source[0]'
                        class='btn btn-danger'>Remove</a>
                </div>
            </div>
        </div>
    </div>
</div>";
}
?>
<div class="col-xs-12 col-md-6 col-lg-4">
    <a href="#addSource" data-toggle="modal" class="add">
        <div class="content">
            <p style="font-size: 120px; font-weight: bolder; margin: 0; color: rgb(210,210,210);">+</p>
        </div>
    </a>
</div>

<div class='modal fade' id='addSource' tabindex='-1' role='dialog' aria-labelledby='addSource' aria-hidden='true'>
    <div class='modal-dialog modal-sm'>
        <div class='modal-content' style='margin-top: 68px;'>
            <div class='modal-header'>
                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                <p class='modal-title'>New Source Feed</p>
            </div>
            <div class='modal-body'>
                <div style='text-align: center;'>
                    <form method='post' action='/Internal_Functions/Functions.php?Function=addSource' class='form-horizontal'>
                        <input type='text' class='form-control input-lg' name='sourceName' placeholder='Name' required />
                        <input type='text' class='form-control input-lg' name='sourceLink' placeholder='Link' required />
                        <button class='btn btn-primary' type='submit' name='addSource'>Add</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>