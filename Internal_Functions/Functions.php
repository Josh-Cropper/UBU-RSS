<?php

include_once 'Connection/_connect.php';

if (isset($_GET['Function'])) {
    $function = $_GET['Function'];

    switch ($function) {
        case "addSource":
            if (isset($_POST['addSource'])) {
                Functions::getInstance()->addSource($_POST['sourceName'], $_POST['sourceLink']);
                header("Location: /index.php/Sources");
            }
        case "removeSource":
            if (isset($_POST['data'])) {
                Functions::getInstance()->removeSource($_POST['data']);
            }
        case "changeUsername":
            if (isset($_POST['changeUsername'])) {
                Functions::getInstance()->changeUsername($_POST['UserID'], $_POST['newUsername'], $_POST['Password']);
                header("Location: /index.php/Account");
            }
        case "changePassword":
            if (isset($_POST['changePassword'])) {
                Functions::getInstance()->changePassword($_POST['UserID'], $_POST['newPassword'], $_POST['Password']);
                header("Location: /index.php/Account");
            }
        case "removeUser":
            if (isset($_POST['userID'])) {
                Functions::getInstance()->removeUser($_POST['userID']);
            }
        case "addUser":
            if (isset($_POST['newUser'])) {
                if ($_POST['Administrator'] == 1) {
                    Functions::getInstance()->addUser($_POST['newUser'], $_POST['Password'], $_POST['Administrator'], $_POST['currentPassword'], $_POST['creatorID']);
                } else {
                    Functions::getInstance()->addUser($_POST['newUser'], $_POST['Password'], $_POST['Administrator'], null, $_POST['creatorID']);
                }
            }
        case "editArticle":
            if (isset($_POST['editArticle'])) {
                Functions::getInstance()->editArticle(
                        $_POST['sourceID'], $_POST['Title'], $_POST['editTitle'], $_POST['Description'], $_POST['Link'], $_POST['Timestamp'], $_POST['editDescription'], $_POST['Image'], $_POST['editImage']);
            }
        case "remArticle":
                if (isset($_POST['Undo'])) {
                    Functions::getInstance()->remArticle(true, $_POST['sourceID'], $_POST['Title']);
                } else if (isset($_POST['remArticle'])) {
                    Functions::getInstance()->remArticle(false, $_POST['sourceID'], $_POST['Title'], $_POST['Description'], $_POST['Image'], $_POST['Link'], $_POST['Timestamp']);
                }
        default :
            return;
    }
}

class Functions {

    protected static $_instance;

    public static final function getInstance() {
        if (!self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    function getConnection() {
        global $connection;

        return $connection;
    }

    function getTable($tablename = null) {
        global $_TABLE;
        if ($tablename == NULL) {
            return $_TABLE;
        }

        return $_TABLE[$tablename];
    }

    function usernameFromUserID($userID) {
        /* $userID is an argument, much like you'd get in Java,
         * $type = 'int', simply defines that the argument must be of type int,
         * for PHP versions 7+, you can also do $userID = NULL, to allow NULL value,
         * and make $userID NULL by default if no argument is given.
         */

        /* $connection is an object in _connect.php,
         * the global command is because a function cannot use a variable outside itself that is not global,
         * the glable command makes the variable internally global.
         * 
         * $_TABLE is an array of table names from _settings.php, which is being included by _connect.php,
         * again, it needs to be turned internally global in order for it to be able to be used.
         */

        $command = "SELECT Username FROM " . $this->getTable('Users') . " WHERE UserID = $userID;"; // The applicable SQL command.
        $query = mysqli_query($this->getConnection(), $command) or die(mysqli_error());
        /* Uses the earlier $connection object to query the database, as this can be not only to pull data,
         * but also to manipulate or add data to the database, it simply creates a temporary object which 
         * can have it's information pulled from it in a manner of ways, with differing commands.
         */

        $result = mysqli_fetch_array($query);
        /* mysqli_fetch_array pulls the next returned row from the database, as an array. It will continue to
         * do this until there are no more returnable rows. There are multiple ways to check if there is another
         * returnable row, but I normally just use if($row = mysqli_fetch_array($query)){}. But here, due to the
         * SQL command for the query, we know there will only be one row at most.
         */

        return $result[0]; // Return the first result of the only array as it's own data type.
    }

    function getSources($num = null) {
        if($num != null){
            $command = "SELECT * FROM " . $this->getTable('Sources') . " ORDER BY SourceID asc LIMIT $num;";
        } else {
            $command = "SELECT * FROM " . $this->getTable('Sources') . " ORDER BY SourceID asc;";
        }
        $query = mysqli_query($this->getConnection(), $command) or die(mysqli_error());
        $result = array();
        while ($return = mysqli_fetch_array($query)) {
            array_push($result, $return);
        }

        return $result;
    }

    function getSource($sourceID) {
        $command = "SELECT * FROM " . $this->getTable('Sources') . " WHERE SourceID = $sourceID;";
        $query = mysqli_query($this->getConnection(), $command) or die(mysqli_error());

        return mysqli_fetch_array($query);
    }

    function getSourceName($sourceID) {
        return $this->getSource($sourceID)[1];
    }

    function getSourceIDs() {
        $command = "SELECT SourceID FROM " . $this->getTable('Sources') . " ORDER BY SourceID asc;";
        $query = mysqli_query($this->getConnection(), $command) or die(mysqli_error());
        $result = array();
        while ($return = mysqli_fetch_array($query)) {
            array_push($result, $return[0]);
        }

        return $result;
    }

    function getArticles($limit = null, $sourceIDs = array()) {
        $articles = array();
        foreach ($sourceIDs as $sourceID) {
            $articles = array_merge($articles, $this->getSourceArticles($sourceID));
        }
        foreach ($articles as $key => $node) {
            $timestamps[$key] = $node[3];
        }
        array_multisort($timestamps, SORT_DESC, $articles);
        if ($limit != null) {
            array_splice($articles, $limit);
        }
        return $articles;
    }

    function getLocalArticles($sourceID) {
        $command = "SELECT * FROM " . $this->getTable('Articles') . " WHERE SourceID = $sourceID;";
        $query = mysqli_query($this->getConnection(), $command);
        $return = array();
        while ($result = mysqli_fetch_array($query)) {
            array_push($return, $result);
        }
        return $return;
    }

    function getSourceArticles($sourceID) {
        $source = simplexml_load_file($this->getSource($sourceID)[3]);
        $localArticles = $this->getLocalArticles($sourceID);
        $result = array();
        foreach ($source->channel->item as $item) {
            $title = str_replace('"', '&quot;', $item->title);
            $description = str_replace('"', '&quot;', $item->description);
            $link = str_replace('&', '&amp;', $item->link);
            $pubDate = $item->pubDate;
            $timestamp = strtotime($pubDate);
            $image = $item->image;
            $matched = false;
            foreach ($localArticles as $localArticle) {
                if ($title == $localArticle[2]) {
                    $matched = true;
                    array_push($result, array($title, $description, $link, $timestamp, $image, $sourceID, true, $localArticle[7], $localArticle[8], $localArticle[9], $localArticle[10]));
                }
            }
            if ($matched == false) {
                array_push($result, array($title, $description, $link, $timestamp, $image, $sourceID, false));
            }
        }
        return $result;
    }

    function getUsers() {
        $command = "SELECT * FROM " . $this->getTable('Users') . " ORDER BY User_ID asc;";
        $query = mysqli_query($this->getConnection(), $command) or die(mysqli_error());
        $result = array();
        while ($return = mysqli_fetch_array($query)) {
            array_push($result, $return);
        }

        return $result;
    }

    function getFeedID($name) {
        $command = "SELECT FeedID FROM " . $this->getTable('Feeds') . " WHERE Name = '$name';";
        $query = mysqli_query($this->getConnection(), $command) or die(mysqli_error());

        $result = mysqli_fetch_array($query)[0];
        return $result;
    }

    function getFeedSources($feedID) {
        $command = "SELECT * FROM " . $this->getTable('Sources') . " WHERE FeedID = '$feedID';";
        $query = mysqli_query($this->getConnection(), $command) or die(mysqli_error());
        $result = array();
        while ($return = mysqli_fetch_array($query)) {
            array_push($result, $return);
        }
        return $result;
    }

    function addSource($name, $link) {
        $command = "INSERT INTO " . $this->getTable('Sources') . "(SourceName, Type, Location) VALUES('$name', 0, '$link');";
        try {
            $query = mysqli_query($this->getConnection(), $command);
        } catch (mysqli_sql_exception $e) {
            $this->throwError("sources", array(false, "Oops, There was an error communicating with the MySQL database!"));
        }
        return true;
    }

    function removeSource($sourceID) {
        $command = "DELETE FROM " . $this->getTable('Sources') . " WHERE SourceID = $sourceID;";
        try {
            $query = mysqli_query($this->getConnection(), $command);
        } catch (mysqli_sql_exception $e) {
            $this->throwError("sources", array(false, "Oops, There was an error communicating with the MySQL database!"));
        }
        $this->throwError("sources", array(true, "Source Successfully Removed!"));
    }

    function changeUsername($userID, $newUsername, $password) {
        $password = hash('sha256', $password);
        $password = stripslashes($password);
        $password = $this->getConnection()->real_escape_string($password);

        $command = "SELECT * FROM " . $this->getTable('Users') . " WHERE UserID = $userID AND Password = '$password';";
        try {
            $query = mysqli_query($this->getConnection(), $command);
        } catch (mysqli_sql_exception $e) {
            return "Error: $e";
        }

        if (mysqli_num_rows($query) == 1) {
            $command = "UPDATE " . $this->getTable('Users') . " SET Username = '$newUsername' WHERE UserID = $userID;";
            try {
                $query = mysqli_query($this->getConnection(), $command);
            } catch (mysqli_sql_exception $e) {
                $this->throwError("account", array(false, "Oops, There was an error communicating with the MySQL database!"));
            }
        } else {
            $this->throwError("account", array(false, "Incorrect Password!"));
        }
        return true;
    }

    function changePassword($userID, $newPassword, $password) {
        $password = hash('sha256', $password);
        $password = stripslashes($password);
        $password = $this->getConnection()->real_escape_string($password);

        $command = "SELECT * FROM " . $this->getTable('Users') . " WHERE UserID = $userID AND Password = '$password';";
        try {
            $query = mysqli_query($this->getConnection(), $command);
        } catch (mysqli_sql_exception $e) {
            $this->throwError("account", array(false, "Oops, There was an error communicating with the MySQL database!"));
        }

        if (mysqli_num_rows($query) == 1) {
            $newPassword = hash('sha256', $newPassword);
            $newPassword = stripslashes($newPassword);
            $newPassword = $this->getConnection()->real_escape_string($newPassword);

            $command = "UPDATE " . $this->getTable('Users') . " SET Password = '$newPassword' WHERE UserID = $userID;";
            try {
                $query = mysqli_query($this->getConnection(), $command);
            } catch (mysqli_sql_exception $e) {
                $this->throwError("account", array(false, "Oops, There was an error communicating with the MySQL database!"));
            }
        } else {
            $this->throwError("account", array(false, "Incorrect Password!"));
        }
        return true;
    }

    function removeUser($userID) {
        $command = "DELETE FROM " . $this->getTable('Users') . " WHERE User_ID = $userID;";
        try {
            $query = mysqli_query($this->getConnection(), $command) or die($this->throwError("account", array(false, $command)));
        } catch (mysqli_sql_exception $e) {
            $this->throwError("account", array(false, "Oops, There was an error communicating with the MySQL database!"));
        }
        $this->throwError("account", array(true, "User Successfully Removed! $userID"));
    }

    function addUser($newUser, $newUserPassword, $admin, $password = null, $creatorID) {
        $newUsername = $newUser;
        $newUsername = stripslashes($newUsername);
        $newUsername = $this->getConnection()->real_escape_string($newUsername);

        $newPassword = hash("sha256", $newUserPassword);
        $newPassword = stripslashes($newPassword);
        $newPassword = $this->getConnection()->real_escape_string($newPassword);

        if ($password != null) {
            $currentPassword = hash("sha256", $password);
            $currentPassword = stripslashes($currentPassword);
            $currentPassword = $this->getConnection()->real_escape_string($currentPassword);

            $command = "SELECT * FROM " . $this->getTable('Users') . " WHERE UserID = '$creatorID' AND Password = '$currentPassword';";
            $query = mysqli_query($this->getConnection(), $command) or die(mysqli_error());

            $correctPassword = mysqli_num_rows($query) == 1 ? true : false;

            if ($correctPassword) {
                $command = "INSERT INTO " . $this->getTable('Users') . " (Username, Password, SuperAdmin) VALUES ('$newUsername', '$newPassword', 1);";
                $query = mysqli_query($this->getConnection(), $command) or die($errorMessage = array(false, "Username Taken!"));
                $this->throwError("account", array(true, "User Added!"));
            } else {
                $password == "" ?
                                $this->throwError("account", array(false, "Password Required!")) :
                                $this->throwError("account", array(false, "Incorrect Password!"));
            }
        } else {
            $command = "INSERT INTO " . $this->getTable('Users') . " (Username, Password, SuperAdmin) VALUES ('$newUsername', '$newPassword', 0);";
            $query = mysqli_query($this->getConnection(), $command) or die($this->throwError("account", array(false, "Username Taken!")));
            $this->throwError("account", array(true, "User Added!"));
        }
    }

    function throwError($page, $error = array()) {
        $_SESSION['Error'] = $error;

        header("Location: http://rss.mcnet.co/index.php/$page");
    }

    function editArticle($sourceID, $Title, $editTitle = null, $Description, $Link, $Timestamp, $editDescription = null, $Image = null, $editImage = null) {
        $command = "SELECT * FROM " . $this->getTable('Articles') . " WHERE SourceID = $sourceID AND Title = '$Title';";
        $query = mysqli_query($this->getConnection(), $command);
        $result = mysqli_num_rows($query);

        if ($result == 0) {
            $command = "INSERT INTO " . $this->getTable('Articles')
                    . " (SourceID, Title, Description, Link, Timestamp, Image, EditTitle, EditDescription, EditImage)"
                    . " VALUES($sourceID, '$Title', '$Description', '$Link', '$Timestamp', '$Image', '$editTitle', '$editDescription', '$editImage');";
        } else {
            $command = "UPDATE " . $this->getTable('Articles')
                    . " SET EditImage = '$editImage', EditDescription = '$editDescription', EditTitle = '$editTitle' WHERE SourceID = $sourceID AND Title = '$Title';";
        }
        $query = mysqli_query($this->getConnection(), $command) or die(mysqli_error());
        $this->throwError("articles", array(true, "Article Edited!"));
    }

    function remArticle($undo, $sourceID, $Title, $Description, $Image, $Link, $Timestamp) {
        $command = "SELECT * FROM " . $this->getTable('Articles') . " WHERE SourceID = $sourceID AND Title = '$Title';";
        $query = mysqli_query($this->getConnection(), $command);
        $check = mysqli_num_rows($query);

        if ($check == 0) {
            $command = "INSERT INTO " . $this->getTable('Articles')
                    . " (SourceID, Title, Description, Link, Timestamp, Image, EditTitle, EditDescription, EditImage, Removed)"
                    . " VALUES($sourceID, '$Title', '$Description', '$Link', '$Timestamp', '$Image', '$Title', '$Description', '$Image', 1);";
        } else {
            if ($undo == true) {
                $command = "UPDATE " . $this->getTable('Articles')
                        . " SET Removed = 0 WHERE SourceID = $sourceID AND Title = '$Title';";
            } else {
                $command = "UPDATE " . $this->getTable('Articles')
                        . " SET Removed = 1 WHERE SourceID = $sourceID AND Title = '$Title';";
            }
        }
        $query = mysqli_query($this->getConnection(), $command) or die(mysqli_error());
        if ($undo) {
            $this->throwError("articles", array(true, "Article Remove Undone!"));
        } else {
            $this->throwError("articles", array(true, "Article Removed!"));
        }
    }

}
