<?php
include_once $_SERVER["DOCUMENT_ROOT"] . '/Internal_Functions/Connection/_connect.php';
include_once $_SERVER["DOCUMENT_ROOT"] . '/Internal_Functions/Functions.php';

$pageTitle = "Dashboard";
$request = '';
$error = null;

if (isset($_SESSION['User'])) {

    if (isset($_SERVER["PATH_INFO"])) {
        $request = ($_SERVER["PATH_INFO"] != null || '' ? $_SERVER["PATH_INFO"] : "Dashboard");
        $request = strtolower($request);
        $request = str_replace("/", "", $request);


        switch ($request) {
            case "account":
                $pageTitle = "Account";
                break;
            case "feed":
                $pageTitle = "Feed";
                break;
            case "articles":
                $pageTitle = "Articles";
                break;
            case "sources":
                $pageTitle = "Sources";
                break;
            default:
                break;
        }
    } else {
        header("Location: /index.php/Articles");
    }

    if (isset($_SESSION['Error'])) {
        $error = $_SESSION['Error'];

        unset($_SESSION['Error']);
    }
} else {
    header("Location: /Login/");
}
?>

<html>
    <head>

        <link rel="stylesheet" type="text/css" href="/Design/Bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/Design/Styling/style.css">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <script type="text/javascript" src="/Design/Bootstrap/js/bootstrap.min.js"></script>

        <title><?php print $pageTitle; ?></title>
    </head>
    <body>
        <div class="container-fluid" style="position: fixed; width: 100%; z-index: 9000;">
            <div class="row topbar">
                <div class="col-xs-1">
                    <a data-toggle="modal" href="#sidebar" class="menu"><span class="glyphicon glyphicon-menu-hamburger" style="color: white; font-size: 30px;" /></a>
                </div>
                <div class="col-xs-10"><h4 class="page-title"><?php print $pageTitle; ?></h4></div>
                <div class="col-xs-1" />
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="col-xs-12" style="border: 0px solid red; margin-top: 68px;">
            
                    <?php
                    if ($error != null) {
                        print "<div class='row'>
                                    <div class='col-xs-12' style='text-align: center; margin: 10px 0px 25px;'>";
                        $error[0] == true ? print "<span class='alert alert-success'>$error[1]</span>" :
                            print "<span class='alert alert-warning'>$error[1]</span>";
                        print "</div></div>";
                    }
                    ?>
        </div>
        <?php
        switch ($pageTitle) {
            case "Account":
                include 'Internal_Functions/Pages/Account.php';
                break;
            case "Feed":
                include 'Internal_Functions/Pages/Feed.php';
                break;
            case "Articles":
                include 'Internal_Functions/Pages/Articles.php';
                break;
            case "Sources":
                include 'Internal_Functions/Pages/Sources.php';
                break;
            default:
                break;
        }
        ?>
    </div>
</div>

<div id='sidebar' class='modal fade' style="margin-top: 53px;">

    <div class="modal-content col-xs-4 col-md-2" style='position: relative; left: 0px; height: 100%; padding: 0;'>
        <div class="modal-body" style=''>
            <ul class="nav nav-pills nav-stacked">
                <!--<li<?php //$pageTitle == "Dashboard" ? print " class='active2'" : ""; ?>><a href="/index.php">Dashboard</a></li>-->
                <li<?php $pageTitle == "Account" ? print " class='active2'" : ""; ?>><a href="/index.php/Account/">Account</a></li>
                <li<?php $pageTitle == "Feed" ? print " class='active2'" : ""; ?>><a href="/RSS.xml">Feed</a></li>
                <li<?php $pageTitle == "Articles" ? print " class='active2'" : ""; ?>><a href="/index.php/Articles/">Articles</a></li>
                <li<?php $pageTitle == "Sources" ? print " class='active2'" : ""; ?>><a href="/index.php/Sources/">Sources</a></li>
                <li class="divider"></li>
                <li><a data-toggle="modal" href="#logout">Log Out</a></li>
            </ul>
        </div>
    </div>

</div>

<div class="modal fade" id="logout" tabindex="-1" role="dialog" aria-labelledby="logout" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style='margin-top: 68px;'>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <p class="modal-title">Are you sure you want to logout?</p>
            </div>
            <div class="modal-body">
                <div style="text-align: center;">
                    <form method="post" action="/Internal_Functions/Account_Control/logout.php">
                        <button class="btn btn-primary" type="submit" name="logout">Logout</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>