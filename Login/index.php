<?php
    include_once '../Internal_Functions/Connection/_connect.php';
    
?>
<html>
    <head>
        <meta charset="UTF-8">
        
        <link rel="stylesheet" type="text/css" href="/Design/Bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/Design/Styling/style.css">
        
        <title>RSS Login</title>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row" style="height: 33%;">
                <div class="col-xs-4"></div>
                <div class="col-xs-4" style="text-align: center; height: 100%;">
                    <h1 style="position: relative; top: 50%; transform: translateY(-50%); font-size: 50px;">Login</h1>
                </div>
                <div class="col-xs-4"></div>
            </div>
            <div class="row" style="height: 67%;">
                <div class="col-xs-3 col-md-5"></div>
                <div class="col-xs-6 col-md-2 content" style="text-align: center; min-height: 0;">
                    <?php
                        if(isset($_SESSION['Error'])){
                            $errorMessage = $_SESSION['Error'][1];
                            switch ($_SESSION['Error'][0]) {
                                case true:
                                    print "<div class='col-xs-12' style='height: 22px;'><span class='label label-success' style='margin: 10px;'>$errorMessage</span></div>";
                                    break;

                                default:
                                    print "<div class='col-xs-12' style='height: 22px;'><span class='label label-warning' style='margin: 10px;'>$errorMessage</span></div>";
                                    break;
                            }
                            unset($_SESSION['Error']);
                        }
                    ?>
                    <form class="form-horizontal login" action="/Internal_Functions/Account_Control/login.php" method="post">
                        <input class="form-control" type="text" placeholder="Username" name="username" required />
                        <input class="form-control" type="password" placeholder="Password" name="password" required />
                        <button class="btn btn-primary" name="login" type="submit">Enter</button>
                    </form>
                </div>
                <div class="col-xs-3 col-md-5"></div>
            </div>
        </div>
    </body>
</html>
