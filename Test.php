<?php ##Unit Testing##
include_once 'Internal_Functions/Functions.php';

if ($_GET['Function'] == "RecreateRSS") {
    header("Content-Type: text/xml");
    print
"<?xml version='1.0' encoding='UTF-8' ?>
<rss version='2.0'>
    <channel>
        <title>BBC News - World</title>  
        <link>http://www.bbc.co.uk/news/world</link>  
        <description>The latest stories from the World section of the BBC News web site.</description>  
        <language>en-gb</language>  
        <lastBuildDate>Tue, 15 Mar 2016 10:54:57 GMT</lastBuildDate>  
        <copyright>Copyright: (C) British Broadcasting Corporation, see http://news.bbc.co.uk/2/hi/help/rss/4498287.stm for terms and conditions of reuse.</copyright>  
        <image> 
            <url>http://news.bbcimg.co.uk/nol/shared/img/bbc_news_120x60.gif</url>  
            <title>BBC News - World</title>  
            <link>http://www.bbc.co.uk/news/world</link>  
            <width>120</width>  
            <height>60</height> 
        </image>";
    
    $source = simplexml_load_file("http://feeds.bbci.co.uk/news/world/rss.xml?edition=uk");
    
    foreach ($source->channel->item as $item){
        $title = $item->title;
        $description = $item->description;
        $link = str_replace('&', '&amp;', $item->link);//Testing showed $item-link strings were providing un-escaped & characters,
                                                       //so this manually replaces them with escaped versions.
        $guid = $item->guid;
        $pubDate = $item->pubDate;
        
        print
"
        <item>
            <title>$title</title>
            <description>$description</description>
            <link>$link</link>
            <guid>$guid</guid>
            <pubDate>$pubDate</pubDate>
        </item>";
    }
    
    print
"
    </channel>
</rss>";
} //Testing recreating RSS feed from remote elements

